<?php


namespace zqscjj\EasyTBK\TaoBao\Request;
use zqscjj\EasyTBK\TaoBao\RequestCheckUtil;

/**
 * TOP API: taobao.tbk.relation.refund
 *
 * @author auto create
 * @since 1.0, 2021.12.10
 */

class TbkRelationRefund
{
    //页数大小
    private $page_size;
    //页码
    private $page_no;
    //1-维权发起时间，2-订单结算时间（正向订单），3-维权完成时间，4-订单创建时间，5-订单更新时间
    private $search_type;
    //	1 表示2方，2表示3方，0表示不限
    private $refund_type;
    //开始时间
    private $start_time;
    //1代表渠道关系id，2代表会员关系id
    private $biz_type;

    private $search_option = [];
    private $apiParas = [];

    public function setPageSize($pageSize)
    {
        $this->page_size = $pageSize;
        $this->search_option['page_size'] = $pageSize;
    }

    public function setPageNo($pageNo)
    {
        $this->page_no = $pageNo;
        $this->search_option['page_no'] = $pageNo;
    }

    public function setSearchType($searchType)
    {
        $this->search_type = $searchType;
        $this->search_option['search_type'] = $searchType;
    }

    public function setRefundType($refundType)
    {
        $this->refund_type = $refundType;
        $this->search_option['refund_type'] = $refundType;
    }

    public function setStartTime($startTime)
    {
        $this->start_time = $startTime;
        $this->search_option['start_time'] = $startTime;
    }

    public function setBizType($bizType)
    {
        $this->biz_type = $bizType;
        $this->search_option['biz_type'] = $bizType;
    }

    public function getApiMethodName()
    {
        return "taobao.tbk.relation.refund";
    }

    public function getApiParas()
    {
        $this->apiParas['search_option'] = json_encode($this->search_option);
        return $this->apiParas;
    }

    public function check()
    {

    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }



}




















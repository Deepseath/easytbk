<?php


namespace zqscjj\EasyTBK\TaoBao\Request;

use zqscjj\EasyTBK\TaoBao\RequestCheckUtil;

/**
 * taobao.tbk.dg.cpa.activity.detail
 * 淘宝客获取CPA活动具体执行效果的明细数据（含预估和结算维度）
 * Class TbkDgCpaActivityDetail
 * @package zqscjj\EasyTBK\TaoBao\Request
 */
class TbkDgCpaActivityDetail
{
    //明细类型，1：预估明细，2：结算明细
    private $queryType;
    //每页条数
    private $pageSize;
    //页码
    private $pageNo;
    //CPA活动ID
    private $eventId;
    //CPA活动奖励的统计口径，相关说明见文档：https://www.yuque.com/docs/share/7ecf8cf1-7f99-4633-a2ed-f9b6f8116af5?#
    private $indicatorAlias;

    private $apiParas = [];

    public function setQueryType($queryType)
    {
        $this->queryType = $queryType;
        $this->apiParas['query_type'] = $queryType;
    }

    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
        $this->apiParas['page_size'] = $pageSize;
    }

    public function setPageNo($pageNo)
    {
        $this->pageNo = $pageNo;
        $this->apiParas['page_no'] = $pageNo;
    }

    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
        $this->apiParas['event_id'] = $eventId;
    }

    public function setIndicatorAlias($indicatorAlias)
    {
        $this->indicatorAlias = $indicatorAlias;
        $this->apiParas['indicator_alias'] = $indicatorAlias;
    }
    public function getApiMethodName()
    {
        return "taobao.tbk.dg.cpa.activity.detail";
    }

    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull ($this->eventId, "event_id");
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}

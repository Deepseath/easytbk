<?php
/**
 * Created by PhpStorm.
 * User: zqscjj
 * Date: 2019/1/8
 * Time: 15:54
 */

namespace zqscjj\EasyTBK\Vip;


interface RequestInterface
{
    public function getMethod();

    public function getParamJson();
}

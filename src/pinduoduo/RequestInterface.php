<?php
/**
 * Created by PhpStorm.
 * User: zqscjj
 * Date: 2019/1/8
 * Time: 15:43
 */

namespace zqscjj\EasyTBK\PinDuoDuo;


interface RequestInterface
{
    public function getParams();
}

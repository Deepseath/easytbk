<?php

namespace zqscjj\EasyTBK\PinDuoDuo\Request;

use zqscjj\EasyTBK\PinDuoDuo\RequestInterface;


class DdkGoodsRecommendGetRequest implements RequestInterface
{

    private $type = 'pdd.ddk.goods.recommend.get';

    //位置不是页码
    private $offset;

    private $limit;

    private $channelType;

    private $listId='';

    private $cateId='';


    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setChannelType($channelType)
    {
        $this->channelType = $channelType;
    }

    public function getChannelType()
    {
        return $this->channelType;
    }

    public function setListId($list_id)
    {
        $this->listId = $list_id;
    }

    public function getListId()
    {
        return $this->listId;
    }

    public function setCateId($cate_id)
    {
        $this->cateId = $cate_id;
    }

    public function getCateId()
    {
        return $this->cateId;
    }


    public function getParams()
    {
        $params = [
            'type' => $this->type,
            'offset' => $this->offset,
            'limit' => $this->limit,
            'channel_type' => intval($this->channelType),
            'list_id' => $this->listId,
        ];
        if (!empty($this->cateId)){
            $params['cat_id'] = $this->cateId;
        }
        return $params;
    }
}

<?php

namespace zqscjj\EasyTBK\PinDuoDuo\Request;

use zqscjj\EasyTBK\PinDuoDuo\RequestInterface;


class DdkGoodsDetailRequest implements RequestInterface
{
    /**
     * 查询多多进宝商品详情
     * @var string
     */
    private $type = 'pdd.ddk.goods.detail';

    /**
     * 商品ID，仅支持单个查询。例如：[123456]
     * @var
     */
    private $goodsIdList;

    //商品goodsSign，支持通过goodsSign查询商品。goodsSign是加密后的goodsId, goodsId已下线，请使用goodsSign来替代。使用说明：https://jinbao.pinduoduo.com/qa-system?questionId=252
    private $goodsSign;

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setGoodsIdList($goodsIdList)
    {
        $this->goodsIdList = $goodsIdList;
    }


    public function getType()
    {
        return $this->type;
    }

    public function getGoodsIdList()
    {
        return $this->goodsIdList;
    }

    public function setGoodsSign($sign)
    {
        $this->goodsSign = $sign;
    }

    public function getGoodsSign()
    {
        return $this->goodsSign;
    }

    public function getParams()
    {
        return [
            'type' => $this->type,
            //'goods_id_list' => $this->goodsIdList
            'goods_sign' => $this->goodsSign,
        ];
    }
}
<?php


namespace zqscjj\EasyTBK\PinDuoDuo\Request;


class DdkResourceUrlGen
{
    public $type = 'pdd.ddk.resource.url.gen';

    protected $pid;

    protected $custom_parameters;

    protected $generate_we_app = "true";

    protected $resource_type;

    protected $url;

    /**
     * @param mixed $pid
     */
    public function setPid($pid)
    {
        $this->pid = $pid;
    }

    /**
     * @param mixed $custom_parameters
     */
    public function setCustomParameters($custom_parameters)
    {
        $this->custom_parameters = is_array($custom_parameters) ? json_encode($custom_parameters) : $custom_parameters;
    }


    public function setResourceType($type)
    {
        $this->resource_type = $type;
    }


    public function setGenerateWeApp($generate_we_app)
    {
        $this->generate_we_app = $generate_we_app;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }



    public function getParams()
    {
        $params = [
            'type' => $this->type,
            'pid' => $this->pid,
            'custom_parameters' => $this->custom_parameters,
            'generate_we_app' => $this->generate_we_app,
            'resource_type' => $this->resource_type,
            'url' => $this->url,
        ];
        return array_filter($params);
    }





}
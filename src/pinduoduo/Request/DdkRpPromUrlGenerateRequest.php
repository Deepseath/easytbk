<?php
/**
 * Created by PhpStorm.
 * User: zqscjj
 * Date: 2018/11/20
 * Time: 下午6:45
 */

namespace zqscjj\EasyTBK\PinDuoDuo\Request;

use zqscjj\EasyTBK\PinDuoDuo\RequestInterface;


class DdkRpPromUrlGenerateRequest implements RequestInterface
{
    /**
     * 生成红包推广链接接口
     * @var string
     */
    private $type = 'pdd.ddk.rp.prom.url.generate';

    /**
     * 推广位ID
     * @var
     */
    private $pid;

    /**
     * 是否生成短链接，true-是，false-否
     * @var
     */
    private $generateShortUrl;

    /**
     * 自定义参数，为链接打上自定义标签。自定义参数最长限制64个字节。
     * @var
     */
    private $customParameters;

    /**
     * 是否生成唤起微信客户端链接，true-是，false-否，默认false
     * @var
     */
    private $generateWeappWebview;


    /**
     * 是否生成小程序推广
     * @var
     */
    private $generateWeApp;

    /**
     * 营销工具类型，必填：
     * -1-活动列表，
     * 0-红包(需申请推广权限)，
     * 2–新人红包，
     * 3-刮刮卡，
     * 5-员工内购，
     * 10-生成绑定备案链接，
     * 12-砸金蛋，
     * 13-一元购C端页面，
     * 14-千万补贴B端页面，
     * 15-充值中心B端页面，
     * 16-千万补贴C端页面，
     * 17-千万补贴投票页面，
     * 18-一元购B端页面，
     * 19-多多品牌星选B端页面，
     * 20-多多品牌星选C端页面，
     * 23-超级红包，
     * 24-礼金全场N折活动B端页面，
     * 25-品牌优选B端页面，
     * 26-品牌优选C端页面，
     * 27-带货赢千万，
     * 28-满减券活动B端页面，
     * 29-满减券活动C端页面，
     * 30-免单B端页面，
     * 31-免单C端页面；
     * 红包推广权限申请流程链接：https://jinbao.pinduoduo.com/qa-system?questionId=289
     * @var
     */
    private $channel_type;


    public function setPid($pid)
    {
        $this->pid = $pid;
    }

    public function getPid()
    {
        return $this->pid;
    }

    public function setGenerateShortUrl($generateShortUrl)
    {
        $this->generateShortUrl = $generateShortUrl;
    }

    public function getGenerateShortUrl()
    {
        return $this->generateShortUrl;
    }

    public function setCustomParameters($customParameters)
    {
        $this->customParameters = $customParameters;
    }

    public function getCustomParameters()
    {
        return $this->customParameters;
    }

    public function setGenerateWeappWebview($generateWeappWebview)
    {
        $this->generateWeappWebview = $generateWeappWebview;
    }

    public function getGenerateWeappWebview()
    {
        return $this->generateWeappWebview;
    }

    public function setGenerateWeApp($generateWeApp)
    {
        $this->generateWeApp = $generateWeApp;
    }

    public function getGenerateWeApp()
    {
        return $this->generateWeApp;
    }

    public function setChannelType($channel_type)
    {
        $this->channel_type = $channel_type;
    }

    public function getChannelType()
    {
        return $this->channel_type;
    }


    public function getParams()
    {
        $params = [
            'type' => $this->type,
            'p_id_list' => $this->pid,
            'generate_short_url' => $this->generateShortUrl,
            'custom_parameters' => $this->customParameters,
            'generate_weapp_webview' => $this->generateWeappWebview,
            'generate_we_app' => $this->generateWeApp,
            'channel_type' => $this->channel_type,
        ];
        return array_filter($params);
    }
}
